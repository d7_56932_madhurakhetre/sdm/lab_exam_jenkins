const express = require("express");
const cors = require("cors");
const db = require("./db");
const pool = require("./db");
const utils = require("./utils");
const app = express();

app.use(express.json());
app.use(cors("*"));

app.get("/books", (req, resp) => {
  // create statement
  const sql = "select * from book";

  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.delete("/book/:id", (req, resp) => {
  const {id}=req.params;
  // create statement
  const sql = `delete from book where book_id='${id}'`;

  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.patch("/book/update", (req, resp) => {
  
  const {id,publisher_name,author_name}=req.body
  // create statement
  const sql = `update book set publisher_name='${publisher_name}', author_name='${author_name}' where book_id='${id}'`;

  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.post("/book/add", (req, resp) => {
  
  const { book_title,publisher_name,author_name}=req.body
  // create statement
  const sql = `insert into book (book_title,publisher_name,author_name) values('${book_title}','${publisher_name}','${author_name}');`;

  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.listen(4444, "0.0.0.0", () => {
  console.log("Server started on port 4444");
});
